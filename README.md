# Vanity

This project publishes [GitLab Pages](https://about.gitlab.com/features/pages/) with
content that provides Vanity URLs for [`go.bagrat.io`](https://go.bagrat.io) golang packages.

## Configuration

The logic is implemented in a simple go application in [`vanity.go`](https://gitlab.com/go.bagrat.io/vanity/blob/master/vanity.go)
that grabs the list of published packages and other information from the
[`vanity.yml`](https://gitlab.com/go.bagrat.io/vanity/blob/master/vanity.yml)
from the same directory.

The configuration file should contain the following information:

* `outDir` - the output directory where the html pages for the packages should be
  generated. Since the pipeline is publishing those files as GitLab Pages, it is
  useful to set it to `public`.
* `indexTemplate` - the HTML go template file to base the index page on.
* `packageTemplate` - the HTML go template file to base the package page on.
* `packages` - the list of packages to publish. Each package item should define
  the following fields:
    * `name` - the name of the package.
    * `url` - the vanity URL of the package.
    * `repo` - the repo URL where the package is actually hosted.

Having the described configuration file inplace, the pipeline defined in
[`.gitlab-ci.yml`](https://gitlab.com/go.bagrat.io/vanity/blob/master/.gitlab-ci.yml)
will generate appropriate HTML files and publish them as GitLab Pages. Adding
a [custom
domain](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html) to your project Pages, you will be able to publish your golang projects under your custom domain name.
