// This tool converts a YAML list of golang packages into a set of
// HTML pages that are possible to host on GitLab Pages and can
// serve as a `go get`-able packages under a custom domain
package main

import (
	"bytes"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"

	"gopkg.in/yaml.v2"
)

const indexHtml = "index.html"

// Configuration is the struct representation of the `vanity.yml`
// configuration file.
type Configuration struct {
	OutDir          string    `yaml:"outDir"`
	PackageTemplate string    `yaml:"packageTemplate"`
	IndexTemplate   string    `yaml:"indexTemplate"`
	Packages        []Package `yaml:"packages"`
}

// ConfigFromFile initializes and returns a new Configuration object
// based on the YAML found in the file pointed by the filename argument.
func ConfigFromFile(filename string) (*Configuration, error) {
	configFile, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer configFile.Close()

	config, err := NewConfiguration(configFile)
	if err != nil {
		return nil, err
	}

	return config, nil
}

// NewConfiguration initializes a ne wConfiguration object, reading the
// YAML contents from the supplied io.Reader.
func NewConfiguration(r io.Reader) (*Configuration, error) {
	contents, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	var config Configuration
	err = yaml.Unmarshal(contents, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func (config *Configuration) render() error {
	err := os.Mkdir(config.OutDir, 0777)
	if err != nil {
		return err
	}

	pkgTemplate, err := readTemplate(config.PackageTemplate)
	if err != nil {
		return err
	}
	config.renderPackages(pkgTemplate)

	indexTemplate, err := readTemplate(config.IndexTemplate)
	if err != nil {
		return err
	}
	config.renderIndex(indexTemplate)

	return nil
}

func (config *Configuration) renderIndex(indexTemplate string) error {
	indexHtml := path.Join(config.OutDir, indexHtml)
	f, err := os.Create(indexHtml)
	if err != nil {
		return err
	}
	defer f.Close()

	t, err := template.New("package").Parse(indexTemplate)
	if err != nil {
		return err
	}

	err = t.Execute(f, config)

	return err
}

func (config *Configuration) renderPackages(pkgTemplate string) error {
	for _, pkg := range config.Packages {
		pkgDir := path.Join(config.OutDir, pkg.Name)
		err := os.Mkdir(pkgDir, 0777)
		if err != nil {
			return err
		}

		pkgHtml := path.Join(pkgDir, indexHtml)
		f, err := os.Create(pkgHtml)
		if err != nil {
			return err
		}
		defer f.Close()

		pkg.render(f, pkgTemplate)
	}

	return nil
}

// Package is a definition of a golang package to be published
// under a custom domain name.
type Package struct {
	Name string `yaml:"name"`
	Url  string `yaml:"url"`
	Repo string `yaml:"repo"`
}

func (p *Package) render(output io.Writer, tmpl string) error {
	t, err := template.New("package").Parse(tmpl)
	if err != nil {
		return err
	}

	err = t.Execute(output, *p)

	return err
}

func readTemplate(filename string) (string, error) {
	tmpl, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer tmpl.Close()

	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(tmpl); err != nil {
		return "", err
	}

	return buf.String(), nil
}

func main() {
	config, err := ConfigFromFile("vanity.yml")
	if err != nil {
		log.Fatal(err)
	}
	config.render()
}
